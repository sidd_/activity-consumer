package com.cleartrip.activityconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityConsumerApplication.class, args);
	}
}

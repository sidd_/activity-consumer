package com.cleartrip.activityconsumer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

	@RequestMapping(value = "health-check")
	public String healthCheck() {
		//log.info("Health check call successful");
		return "Success - activities-consumer is up";
	}
}
